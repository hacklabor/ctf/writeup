## easteregg

Dangeresque likes easter eggs.http://static.ctf.umasscybersec.org/rev/9996d2d0-60d0-476c-9c85-a3b053f0a358/adventureCreated by `Jakob#9448`

File: [adventure](./files/adventure)

## Main Function

The do-while loop with all the IF THEN ELSE is the game part and it will never ends. More interesting is part under the loop this will never reached.

But here you find the Flag XOR decrypt function.

![code](./asset/code.png)

LHEIBZNXEKQSAPHHUWTQ Points the begin of the Key

![code](./asset/key.png)

COJASZQHPZXKLAPHRHOK Points to a address and this address points to the begin of the decrypted flag

![code](./asset/Flagdecrypt.png)

now XOR decrypted Flag and Key in Cyberchef [https://gchq.github.io/CyberChef/ ](https://gchq.github.io/CyberChef/) and you get the flag

![code](./asset/flag.png)

UMASS{m0m_100k_i_can_r3ad_ass3mb1y}





----

## Team: Hacklabor; Time to solve appr. 1 h

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />