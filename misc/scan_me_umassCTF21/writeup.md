## Scan Me

The top layer is a lie.http://static.ctf.umasscybersec.org/misc/8e0111c9-d8d0-4518-973d-dbdcbd9d5a42/scan_me.xcf

Created by `Seltzerz#6678`

File: [scan_me.xcf](./files/scan_me.xcf)

----

- open the File with GIMP
- delete the Top Layer
- Fill the destroyed area with white
- copy one of the black rectangle to the filled area
- scan the QR code 
- open the url [https://imgur.com/a/57VgQ8M](https://imgur.com/a/57VgQ8M)

<img src="./asset/6.png" style="zoom:125%;" />



<img src="./asset/7.png" style="zoom:50%;" />



<img src="./asset/8.png" style="zoom:50%;" />

<img src="./asset/9.png" style="zoom:50%;" />

<img src="./asset/flag.png" style="zoom:50%;" />

----

## Team: Hacklabor; Time to solve appr. 1 h

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />