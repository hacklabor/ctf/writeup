## Justin 2

My friend is in danger and this was the only picture he could send me. Can you find the name of the street he is on? ex. UMDCTF-{Memory_Lane}`https://drive.google.com/drive/folders/1nfFmXuEMRuCOY6BvAAox1AWQPSCqgIhn?usp=sharing`author: `itsecgary`score: `5/10`

File: ![image.PNG](./files/image.PNG)



- You can found this SIGN on the right house
- ![sign](./asset/sign.png)
- I use Yandex.com and put in the first 4 letters the rest you can guess from the auto complete. To type in you can use [https://russian.typeit.org/](https://russian.typeit.org/)
- ![sign](./asset/img1.png)
- use google.maps and search for жилфонд
- ![sign](./asset/img2.png)
- a lot of them are in Novosirbirsk
- now i am looking for a stadium close by
- ![sign](./asset/img3.png)
- the rest is a little bit street view
- https://www.google.com/maps/@55.0367422,82.9267614,3a,75y,23.55h,91.39t/data=!3m6!1e1!3m4!1s5o8NN59Tgqb1gaDzWK4fwQ!2e0!7i13312!8i6656
- ![sign](./asset/img4.png)
- FLAG: UMDCTF-{Ulitsa_Kamenskaya}











----

## Team: Hacklabor; Time to solve appr. 1 h

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />
