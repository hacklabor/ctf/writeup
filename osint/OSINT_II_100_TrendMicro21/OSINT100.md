### Challenge

###### Category: OSINT-II & CRYPTO

###### Points: 100

**Your Mission**, Should you choose to **Accept** it.

A prominent CEO of a security company named Juan Magkape have been reported to be missing for a few years.

It was rumored that the CEO have been part of government conspiracy theories.

Lately a video surfaced online showing an abduction of a foreigner.

The video have been captured on video by a netizen.

Allegedly, there are reports that the abducted foreigner was the CEO in hiding.

**Your goal is to find the Flag somewhere in the location where the reported abduction have occurred.**

The reported video clip have been seen in this link. (**/watch?v=sgQs4ac2D9A**)


## Notes
https://www.youtube.com/watch?v=sgQs4ac2D9A

YT '# Marites Vlog - guy taken by force with gun to abandoned building'

### Juan Magkape
### Marites Vlog
https://www.facebook.com/profile.php?id=100070830924004

![](Pasted%20image%2020210918070409.png)

https://drive.google.com/drive/folders/1_1VerSv-RApYY7tRrfw-tgroqQGbd-ER?fbclid=IwAR2QxM6Vn1XmBdoZNtAe_BjH1uVjzaP6El8i5YmTuJeVra-9AGwziyjgibg

you can found the geocordinates in the exif data of the pictures  

http://exif.regex.info/exif.cgi

14.676476, 121.118213


https://www.google.com/maps/contrib/101453131906042803926/place/ChIJGfFGwti7lzMRoS3Auxed_sQ/@14.6764377,121.118212,17z/data=!4m6!1m5!8m4!1e1!2s101453131906042803926!3m1!1e1?hl=en-DE


![](Pasted%20image%2020210918075101.png)



#### Flag

TMCTF{F1nd1n6R!ck}

## Team: Hacklabor; Time to solve appr. 1.0 h

## [hacklabor.de](hacklabor.de)
<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />


