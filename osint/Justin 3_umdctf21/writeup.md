# Justin 3

### 300

Justin only gave us this camera's live feed and said "Hey guys look! I'm on TV!" Can you find the name of the street he's on?format: UMDCTF-{Memory_Lane}we do not own any IP found besides the one listed below ```curl http://chals5.umdctf.io:8000 author: itsecgary```



- you will get a live web-cam stream

- ![sign](./asset/img1.png)

- The name of the city "Ufa" is written on the advertising poster in the front.

- open google.maps search for UFA and собетская пл ......

- ![sign](./asset/img3.png)

- ![sign](./asset/img2.png)

- a crosscheck with the map on the windows says "Yes" we are right here.

- [https://www.google.com/maps/place/Sovetskaya+ploschad/@54.7216236,55.9469457,3a,75y,295.92h,105.76t/data=!3m6!1e1!3m4!1szL1kkhwkPvtJTaAR01qlhA!2e0!7i13312!8i6656!4m10!1m2!2m1!1z0KPRhNCwLCBSdXNzaWEg0YHQvtCx0LXRgtGB0LrQsNGP!3m6!1s0x43d93a5a3d72c629:0xa78d69fd6de48495!8m2!3d54.72169!4d55.946646!14m1!1BCgIgARICCAI](https://www.google.com/maps/place/Sovetskaya+ploschad/@54.7216236,55.9469457,3a,75y,295.92h,105.76t/data=!3m6!1e1!3m4!1szL1kkhwkPvtJTaAR01qlhA!2e0!7i13312!8i6656!4m10!1m2!2m1!1z0KPRhNCwLCBSdXNzaWEg0YHQvtCx0LXRgtGB0LrQsNGP!3m6!1s0x43d93a5a3d72c629:0xa78d69fd6de48495!8m2!3d54.72169!4d55.946646!14m1!1BCgIgARICCAI)

  

  

  # **FLAG: UMDCTF-{Ulitsa_Pushkina}**









----

## Team: Hacklabor; Time to solve appr. 40min

## [hacklabor.de](hacklabor.de)

<img src="https://hacklabor.de/assets/img/logo/Logo_Large_black.svg" width="200" height="200" />